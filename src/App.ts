import Sum from './Components/Sum';
import Subtract from './Components/Subtract';

let summer = new Sum();
let subtractor = new Subtract(); 

console.log(`The sum of 2 + 1 = ${summer.calculate(2, 1)}`);
console.log(`The subtraction of 15 - 5 = ${subtractor.calculate(15, 5)}`);