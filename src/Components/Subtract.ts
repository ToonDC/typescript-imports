class Subtract {
    
    constructor() {
        
    }

    calculate(firstValue : number, secondValue : number) {
        return firstValue - secondValue;
    }
}

export default Subtract;