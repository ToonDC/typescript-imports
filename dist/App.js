/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	"use strict";
	var Sum_1 = __webpack_require__(1);
	var Subtract_1 = __webpack_require__(2);
	var summer = new Sum_1["default"]();
	var subtractor = new Subtract_1["default"]();
	console.log("The sum of 2 + 1 = " + summer.calculate(2, 1));
	console.log("The subtraction of 15 - 5 = " + subtractor.calculate(15, 5));


/***/ },
/* 1 */
/***/ function(module, exports) {

	"use strict";
	var Sum = (function () {
	    function Sum() {
	    }
	    Sum.prototype.calculate = function (firstValue, secondValue) {
	        return firstValue + secondValue;
	    };
	    return Sum;
	}());
	exports.__esModule = true;
	exports["default"] = Sum;


/***/ },
/* 2 */
/***/ function(module, exports) {

	"use strict";
	var Subtract = (function () {
	    function Subtract() {
	    }
	    Subtract.prototype.calculate = function (firstValue, secondValue) {
	        return firstValue - secondValue;
	    };
	    return Subtract;
	}());
	exports.__esModule = true;
	exports["default"] = Subtract;


/***/ }
/******/ ]);