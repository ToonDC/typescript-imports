module.exports = {  
  entry: './src/App.ts',
  output: {
    path: __dirname + "/dist",
    filename: 'App.js'
  },
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.js']
  },
  module: {
    loaders: [
      { test: /\.ts$/, loader: 'ts-loader' }
    ]
  }
}